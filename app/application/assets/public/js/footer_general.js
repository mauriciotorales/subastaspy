 jQuery(document).ready(function() {
        	
        	 $( "#subscription_form" ).validate({
            
                    /* @validation states + elements 
                    ------------------------------------------- */
             
                    errorClass: "state-error",
                    validClass: "state-success",
                    errorElement: "em",
                    
                    /* @validation rules 
                    ------------------------------------------ */
                        
                    rules: {
						 EmailNewsLetter: {
                                    required: true,
                                      email: true
                            }
                                      
                                                                                                                      
                        
                    },
                    
                    /* @validation error messages 
                    ---------------------------------------------- */
                        
                    messages:{
						 EmailNewsLetter: {
                                     required: 'Enter Email Address',
                                    email: 'Enter a valid Email Address'
									
                            }
                                                                                                  
                     
                    },

                    /* @validation highlighting + error placement  
                    ---------------------------------------------------- */ 
                    
                    highlight: function(element, errorClass, validClass) {
                            $(element).closest('.field').addClass(errorClass).removeClass(validClass);
                    },
                    unhighlight: function(element, errorClass, validClass) {
                            $(element).closest('.field').removeClass(errorClass).addClass(validClass);
                    },
                    errorPlacement: function(error, element) {
                       if (element.is(":radio") || element.is(":checkbox")) {
                                element.closest('.option-group').after(error);
                       } else {
                                error.insertAfter(element.parent());
                       }
                    }
                            
            }); 
            
   		 $( "#login_form" ).validate({
            
                    /* @validation states + elements 
                    ------------------------------------------- */
             
                    errorClass: "state-error",
                    validClass: "state-success",
                    errorElement: "em",
                    
                    /* @validation rules 
                    ------------------------------------------ */
                        
                    rules: {
						 EmailAddress: {
                                    required: true
                            },
                          Password: {
                                    required: true
                            }                
                                                                                                                      
                        
                    },
                    
                    /* @validation error messages 
                    ---------------------------------------------- */
                        
                    messages:{
						 EmailAddress: {
                                    required: 'Enter E-mail or username ',
									
                            },
                             Password: {
                                    required: 'Enter Password',
									
                            },
                                                                                                  
                     
                    },

                    /* @validation highlighting + error placement  
                    ---------------------------------------------------- */ 
                    
                    highlight: function(element, errorClass, validClass) {
                            $(element).closest('.field').addClass(errorClass).removeClass(validClass);
                    },
                    unhighlight: function(element, errorClass, validClass) {
                            $(element).closest('.field').removeClass(errorClass).addClass(validClass);
                    },
                    errorPlacement: function(error, element) {
                       if (element.is(":radio") || element.is(":checkbox")) {
                                element.closest('.option-group').after(error);
                       } else {
                                error.insertAfter(element.parent());
                       }
                    }
                            
            });  
		});
		