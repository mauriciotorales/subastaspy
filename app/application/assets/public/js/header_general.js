$(document).ready(function()
{
	$(".sb-toggle-right").click(function(){
		
        $(".sb-slidebar").toggleClass( "open" );
		$(".sb-toggle-right").toggleClass( "open" );
    });
	
	$(".login-bar").click(function(){
        $(".user-nav").addClass( "open" );
    });
	$(".user-nav-close").click(function(){
        $(".user-nav").removeClass( "open" );
    });
    
	$(".Liked").click(function()
	{
		if($(this).hasClass("Liked"))
		{
			var SpanCount=parseInt($(this).attr("Count")) + 1;
			$(this).attr("Count",SpanCount);
			$(this).addClass("voted" );
			$(this).removeClass("Liked" );
			$(this).addClass("AlreadyLiked" );
			$(this).html(SpanCount);
			
			var dataString = 'AppID='+$(this).attr("AppID");
			
			$.ajax({
			type: "POST",
			url: base_url+"app_likes/add_like",
			data: dataString,
			dataType: "json",
			cache: false,
			//beforeSend: function(){ $("#login").val('Connecting...');},
			success: function(data){
				}
			});
		}
	});
	
	
	redirect_url=base_url;
	
	$('#Login').click(function()
	{
		var EmailAddress=$("#EmailAddress").val();
		var Password=$("#Password").val();
		var dataString = 'EmailAddress='+EmailAddress+'&Password='+Password;
		
		if($.trim(EmailAddress).length>0 && $.trim(Password).length>0)
		{
			$.ajax({
			type: "POST",
			url: base_url+"user/login",
			data: dataString,
			dataType: "json",
			cache: false,
			//beforeSend: function(){ $("#login").val('Connecting...');},
			success: function(data){
				if(data.IsValid=="Yes")
				{
					if(data.IsVerify=="Yes")
					{
						if(data.FirstTimeLogin=="Yes")
						{
							window.location.href =base_url+"view_profile"
						}
						else
						{
							window.location.href =redirect_url ;
						}
					}
					else
					{
						$(".LoginError").show();
						$(".LoginError").text('Please verify your account');
					}
					
				}
				else
				{
					$(".LoginError").show();
					$(".LoginError").text('Incorrect E-mail, username or password');
				}
			 }
		  });
		}
		else
		{
			
		}
	});
	
	$('#subscription_form').submit(function()
	{
		//alert(this.val());
		if(isEmail($("#EmailNewsLetter").val()))
		{
			var dataString = 'EmailAddress='+$("#EmailNewsLetter").val();
			
			$.ajax({
			type: "POST",
			url: base_url+"user/add_news_letter",
			data: dataString,
			dataType: "json",
			cache: false,
			//beforeSend: function(){ $("#login").val('Connecting...');},
			success: function(data){
				if(data.IsValid=="Success")
				{
					$("#EmailNewsLetter").val('');
					$(".SubcribeUser").text("Thank you for your subscription.");
					$(".SubcribeUser").show();
				}
				else
				{
					$(".SubcribeUser").text("You are Already Subscribed!");
					$(".SubcribeUser").show();
				}
			 }
		  });
		}
	});
	
	function isEmail(email) {
 	 var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  	return regex.test(email);
}

		
});